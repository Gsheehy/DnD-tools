# import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt

# Absolute basics


def rolldice(d, n=1):
    rolls = np.random.randint(1, d + 1, size=n)
    return rolls.sum()

# Attacker and Defender classes


class defender():
    def __init__(self, AC, damage_reduction=None, evasion_bonus=None,
                 constitution=None):
        self.ac = AC
        self.dmgred = damage_reduction
        self.evasion = evasion_bonus
        self.constitution = constitution


class attacker():
    def __init__(self, attack_bonus, damage_dice, dice_number, damage_bonus):
        self.abonus = attack_bonus
        self.ddice = damage_dice
        self.ndice = dice_number
        self.dbonus = damage_bonus

    def hit_regular(self, defender):
        '''
            Checks if there is a hit.
            returns an int;
                0 - missed
                1 - hit
                2 - crit
        '''
        # Regular rules
        roll = rolldice(20)
        if roll == 20:
            touch = 2
        elif (roll + self.abonus) >= defender.ac:
            touch = 1
        else:
            touch = 0

        return touch

    def damage_regular(self, defender, crit=False):
        damage = 0
        if crit:
            damage = rolldice(self.ddice, 2 * self.ndice) + self.dbonus
        else:
            damage = rolldice(self.ddice, self.ndice) + self.dbonus

        return damage

    def attack_regular(self, defender):
        damage = 0
        touch = self.hit_regular(defender)
        if touch == 2:
            # CRIT!
            damage = self.damage_regular(defender, crit=True)
        elif touch == 1:
            # successful attack
            damage = self.damage_regular(defender, crit=False)

        return damage

    def hit_modified(self, defender):
        '''
            Checks if there is a hit.
            returns an int;
                0 - missed
                1 - hit
                2 - crit
        '''
        # Modified rules
        roll = rolldice(20)
        if roll == 20:
            touch = 2
        elif (roll + self.abonus) >= (10 + defender.evasion):
            touch = 1
        else:
            touch = 0

        return touch

    def damage_modified(self, defender, crit=False):
        damage = 0
        if crit:
            # CRIT!
            damage = max(rolldice(self.ddice, 2 * self.ndice) +
                         self.dbonus - defender.dmgred, 0)
        else:
            # successful attack
            damage = max(rolldice(self.ddice, self.ndice) +
                         self.dbonus - defender.dmgred, 0)

        return damage

    def attack_modified(self, defender):
        damage = 0
        touch = self.hit_modified(defender)
        if touch == 2:
            # CRIT!
            damage = self.damage_modified(defender, crit=True)
        elif touch == 1:
            # successful attack
            damage = self.damage_modified(defender, crit=False)

        return damage

# Simulation functions


def sim_damage(attack, defender, nrun=10000):
    damages = []
    for _ in range(nrun):
        damage = attack(defender)
        damages.append(damage)
    damages = np.array(damages)
    return damages


def concentration_save(damage, constitution):
    savedc = max(10, damage // 2)
    saved = rolldice(20) + constitution >= savedc
    return saved


def sim_concentration(hit_fun, damage_fun, defender, nrun=1000, maxturn=20):
    turns = []
    for _ in range(nrun):
        concentration = True
        turn = 0
        while concentration:
            if hit_fun(defender) > 0:
                damage = damage_fun(defender)
                concentration = concentration_save(damage,
                                                   defender.constitution)
            turn += 1
            if turn == maxturn:
                # To prevent infinity (or large numbers)
                concentration = False

        turns.append(turn)
    turns = np.array(turns)

    return turns

# Plotting functions


def plotDamageOutcome(abonus, ddice, dnum, dbonus, AC, dred, ebonus):
    # Creating Attacker and Defender
    A = attacker(abonus, ddice, dnum, dbonus)
    D = defender(AC, dred, ebonus)

    # Simulation of damages
    default_damages = sim_damage(A.attack_regular, D)
    mod_damage = sim_damage(A.attack_modified, D)

    # Plotting stuff
    plt.figure(figsize=[10, 5], dpi=96)

    # Default rules
    bins = range(dnum * ddice + dbonus + 2)
    y, _, _ = plt.hist(default_damages, bins=bins, histtype='stepfilled',
                       label='Default rules', color='tab:blue', alpha=0.5,
                       density=1)
    plt.plot([default_damages.mean() + 0.5, default_damages.mean() + 0.5],
             [0, y.max()], color='tab:blue',
             label='Average = {}'.format(default_damages.mean()))
    plt.plot([np.median(default_damages) + 0.5,
              np.median(default_damages) + 0.5],
             [0, y.max()], '--',
             color='tab:blue',
             label='Median = {}'.format(np.median(default_damages)))

    # Modified rules
    plt.hist(mod_damage, bins=bins, histtype='stepfilled',
             label='Modified rules', color='tab:red', alpha=0.5, density=1)
    plt.plot([mod_damage.mean() + 0.5, mod_damage.mean() + 0.5], [0, y.max()],
             color='tab:red',
             label='Average = {}'.format(mod_damage.mean()))
    plt.plot([np.median(mod_damage) + 0.5, np.median(mod_damage) + 0.5],
             [0, y.max()], '--', color='tab:red',
             label='Median = {}'.format(np.median(mod_damage)))

    plt.xlabel('Damage')
    plt.ylabel('Normalized Frequency')
    plt.xticks(ticks=np.array(bins) + 0.5, labels=bins)
    plt.legend()
    plt.tight_layout()


def plotConstiOutcome(abonus, ddice, dnum, dbonus, AC, dred, ebonus, consti):
    # Creating Attacker and Defender
    A = attacker(abonus, ddice, dnum, dbonus)
    D = defender(AC, dred, ebonus, consti)

    # Simulation of concentration saves
    default_turns = sim_concentration(A.hit_regular, A.damage_regular, D)
    mod_turns = sim_concentration(A.hit_modified, A.damage_modified, D)

    # Plotting stuff
    plt.figure(figsize=[10, 5], dpi=96)

    # Default rules
    bins = range(max(default_turns.max(), mod_turns.max()) + 2)
    print(bins)
    y, _, _ = plt.hist(default_turns, bins=bins, histtype='stepfilled',
                       label='Default rules', color='tab:blue', alpha=0.5,
                       density=1)
    plt.plot([default_turns.mean() + 0.5, default_turns.mean() + 0.5],
             [0, y.max()], color='tab:blue',
             label='Average = {}'.format(default_turns.mean()))
    plt.plot([np.median(default_turns) + 0.5,
              np.median(default_turns) + 0.5],
             [0, y.max()], '--',
             color='tab:blue',
             label='Median = {}'.format(np.median(default_turns)))

    # Modified rules
    plt.hist(mod_turns, bins=bins, histtype='stepfilled',
             label='Modified rules', color='tab:red', alpha=0.5, density=1)
    plt.plot([mod_turns.mean() + 0.5, mod_turns.mean() + 0.5], [0, y.max()],
             color='tab:red',
             label='Average = {}'.format(mod_turns.mean()))
    plt.plot([np.median(mod_turns) + 0.5, np.median(mod_turns) + 0.5],
             [0, y.max()], '--', color='tab:red',
             label='Median = {}'.format(np.median(mod_turns)))

    plt.xlabel('Number of attacks to lose concentration')
    plt.ylabel('Normalized Frequency')
    plt.xticks(ticks=np.array(bins) + 0.5, labels=bins)
    plt.legend()
    plt.tight_layout()
